FROM registry.gitlab.com/stephane.ludwig/docker-ansible
LABEL org.opencontainers.image.authors="Stéphane Ludwig <gitlab@stephane-ludwig.net>"

RUN ansible-galaxy collection install checkmk.general
